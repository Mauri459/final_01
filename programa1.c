#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main() {
  double num;

  printf("Escriba un numero: ");
  scanf("%lf",&num );

   printf ("El coseno de %f\n", cos(num));
   printf ("El seno de %f\n", sin(num));
   printf ("El tangente de %f\n", tan(num));

  return 0;
}
